<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='pkreclass'>

  <refmeta>
    <refentrytitle>pkreclass</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>pkreclass</refname>
    <refpurpose>replace pixel values in raster image</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>pkreclass</command>
      <arg choice='plain'><option>-i</option> <replaceable>input</replaceable></arg>
      <arg choice='opt'>
        <option>-c</option> <replaceable>from</replaceable>
        <option>-r</option> <replaceable>to</replaceable>
      </arg>
      <arg choice='plain'><option>-o</option> <replaceable>output</replaceable></arg>
      <arg choice='opt'><replaceable>options</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>pkreclass</command> is a program to replace pixel values in
      raster images.
    </para>
  </refsect1>

  <refsect1 id='options'>
    <title>OPTIONS</title>
    <variablelist>

      <varlistentry>
        <term><option>-i</option> <replaceable>filename</replaceable></term>
        <term><option>--input</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Input image
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-m</option> <replaceable>mask</replaceable></term>
        <term><option>--mask</option> <replaceable>mask</replaceable></term>
        <listitem>
          <para>
            Mask image(s)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-msknodata</option> <replaceable>value</replaceable></term>
        <term><option>--msknodata</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Mask value(s) where image has nodata.
            Use one value for each mask, or multiple values for a single mask.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-nodata</option> <replaceable>value</replaceable></term>
        <term><option>--nodata</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            nodata value to put in image if not valid (0)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-code</option> <replaceable>filename</replaceable></term>
        <term><option>--code</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Recode text file (2 columns: from to)
          </para>
        </listitem>
      </varlistentry>
      
      <varlistentry>
        <term><option>-c</option> <replaceable>classes</replaceable></term>
        <term><option>--class</option> <replaceable>classes</replaceable></term>
        <listitem>
          <para>
            list of classes to reclass (in combination with reclass option)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-r</option> <replaceable>classes</replaceable></term>
        <term><option>--reclass</option> <replaceable>classes</replaceable></term>
        <listitem>
          <para>
            list of recoded classes (in combination with class option)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-ct</option> <replaceable>filename</replaceable></term>
        <term><option>--ct</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            color table
            (file with 5 columns: id R G B ALFA (0: transparent, 255: solid))
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-o</option> <replaceable>filename</replaceable></term>
        <term><option>--output</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            Output mask file
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-ot</option> <replaceable>type</replaceable></term>
        <term><option>--otype</option> <replaceable>type</replaceable></term>
        <listitem>
          <para>
            Data type for output image
            ({Byte / Int16 / UInt16 / UInt32 / Int32 / Float32 / Float64 / CInt16 / CInt32 / CFloat32 / CFloat64}).
            Empty string: inherit type from input image
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-of</option> <replaceable>format</replaceable></term>
        <term><option>--oformat</option> <replaceable>format</replaceable></term>
        <listitem>
          <para>
            Output image format (see also
            <citerefentry>
              <refentrytitle>gdal_translate</refentrytitle>
              <manvolnum>1</manvolnum>
            </citerefentry>).
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-b</option> <replaceable>band</replaceable></term>
        <term><option>--band</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            band index(es) to replace (other bands are copied to output)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-n</option> <replaceable>name</replaceable></term>
        <term><option>--fname</option> <replaceable>name</replaceable></term>
        <listitem>
          <para>
            field name of the shape file to be replaced
            default: label
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-co</option> <replaceable>option</replaceable></term>
        <term><option>--co</option> <replaceable>option</replaceable></term>
        <listitem>
          <para>
            Creation option for output file.
            Multiple options can be specified.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-d</option> <replaceable>description</replaceable></term>
        <term><option>--description</option> <replaceable>description</replaceable></term>
        <listitem>
          <para>
            Set image description
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-v</option> <replaceable>level</replaceable></term>
        <term><option>--band</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            Band index(es) to extract.
            Leave empty to use all bands 
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-v</option> <replaceable>level</replaceable></term>
        <term><option>--verbose</option> <replaceable>level</replaceable></term>
        <listitem>
          <para>
            Verbose mode if &gt; 0
          </para>
        </listitem>
      </varlistentry>

    </variablelist>
    
  </refsect1>

  <refsect1 id='example'>
    <title>EXAMPLE</title>

    <example>
      <screen>
<command>pkreclass</command> <option>-i</option> <replaceable>input1.tif</replaceable> <option>-o</option> <replaceable>output.tif</replaceable> <option>-c</option> <replaceable>1</replaceable> <option>-r</option> <replaceable>0</replaceable> <option>-c</option> <replaceable>2</replaceable> <option>-r</option> <replaceable>0</replaceable>
      </screen>
      <para>
        replace pixel values 1 and 2 with value 0
      </para>
    </example>

    <example>
      <screen>
<command>pkreclass</command> <option>-i</option> <replaceable>vector.shp</replaceable> <option>-o</option> <replaceable>output.shp</replaceable> <option>-c</option> <replaceable>FROM</replaceable> <option>-r</option> <replaceable>TO</replaceable> <option>-n</option> <replaceable>INFIELD</replaceable>
      </screen>
      <para>
        replace <replaceable>FROM</replaceable> with
        <replaceable>TO</replaceable> in field
        <replaceable>INFIELD</replaceable> (of type string) in vector file
        <filename>vector.shp</filename> and write to new vector
        <filename>output.shp</filename>
      </para>
    </example>

    <example>
      <screen>
for((i=0;i&lt;256;++i));do if(($i&lt;100));then echo "$i 1";else echo "$i 0";fi;done &gt; <replaceable>code.txt</replaceable>; <command>pkreclass</command> <option>-i</option> <replaceable>input1.tif</replaceable> <option>-o</option> <replaceable>output.tif</replaceable> <option>--code</option> <replaceable>code.txt</replaceable>
      </screen>
      <para>
        replace all values smaller than 100 with 1, all other values with 0
      </para>
    </example>

    <example>
      <screen>
<command>pkreclass</command> <option>-i</option> <replaceable>input1.tif</replaceable> <option>-o</option> <replaceable>output.tif</replaceable> $(for((i=0;i&lt;256;++i));do if(($i&lt;100));then echo <option>-n</option> " <option>-c</option> $i " " <option>-r</option> 1";else echo " <option>-c</option> $i " " <option>-r</option> 0";fi;done)
      </screen>
      <para>
        same as previous but without temporary file
      </para>
    </example>

  </refsect1>

</refentry>
