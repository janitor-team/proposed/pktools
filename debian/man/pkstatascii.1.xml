<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='pkstatascii'>

  <refmeta>
    <refentrytitle>pkstatascii</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>pkstatascii</refname>
    <refpurpose>program to calculate basic statistics from text file</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>pkstatascii</command>
      <arg choice='plain'><option>-i</option> <replaceable>input</replaceable></arg>
      <arg choice='opt'><option>-c</option> <replaceable>column</replaceable></arg>
      <arg choice='opt'><replaceable>options</replaceable></arg>
      <arg choice='opt'><replaceable>advanced options</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>pkstatascii</command> calculates basic statistics of a data
      series in a text file.
    </para>
  </refsect1>

  <refsect1 id='options'>
    <title>OPTIONS</title>
    <variablelist>

      <varlistentry>
        <term><option>-i</option> <replaceable>filename</replaceable></term>
        <term><option>--input</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            name of the input text file
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-size</option></term>
        <term><option>--size</option></term>
        <listitem>
          <para>
            sample size
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-rnd</option> <replaceable>number</replaceable></term>
        <term><option>--rnd</option> <replaceable>number</replaceable></term>
        <listitem>
          <para>
            generate random numbers
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-dist</option> <replaceable>function</replaceable></term>
        <term><option>--dist</option> <replaceable>function</replaceable></term>
        <listitem>
          <para>
            distribution for generating random numbers, see
            http://www.gn/software/gsl/manual/gsl-ref_toc.html#TOC320
            (only uniform and Gaussian supported yet)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-rnda</option> <replaceable>value</replaceable></term>
        <term><option>--rnda</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            first parameter for random distribution
            (mean value in case of Gaussian)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-rndb</option> <replaceable>value</replaceable></term>
        <term><option>--rndb</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            second parameter for random distribution
            (standard deviation in case of Gaussian)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-mean</option></term>
        <term><option>--mean</option></term>
        <listitem>
          <para>
            calculate mean
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-median</option></term>
        <term><option>--median</option></term>
        <listitem>
          <para>
            calculate median
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-var</option></term>
        <term><option>--var</option></term>
        <listitem>
          <para>
            calculate variance
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-stdev</option></term>
        <term><option>--stdev</option></term>
        <listitem>
          <para>
            calculate standard deviation
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-skew</option></term>
        <term><option>--skewness</option></term>
        <listitem>
          <para>
            calculate skewness
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-kurt</option></term>
        <term><option>--kurtosis</option></term>
        <listitem>
          <para>
            calculate kurtosis
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-sum</option></term>
        <term><option>--sum</option></term>
        <listitem>
          <para>
            calculate sum of column
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-mm</option></term>
        <term><option>--minmax</option></term>
        <listitem>
          <para>
            calculate minimum and maximum value
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-min</option></term>
        <term><option>--min</option></term>
        <listitem>
          <para>
            calculate minimum value
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-max</option></term>
        <term><option>--max</option></term>
        <listitem>
          <para>
            calculate maximum value
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-hist</option></term>
        <term><option>--hist</option></term>
        <listitem>
          <para>
            calculate histogram
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-hist2d</option></term>
        <term><option>--hist2d</option></term>
        <listitem>
          <para>
            calculate 2-dimensional histogram based on two columns
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-nbin</option> <replaceable>value</replaceable></term>
        <term><option>--nbin</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            number of bins to calculate histogram
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-rel</option></term>
        <term><option>--relative</option></term>
        <listitem>
          <para>
            use percentiles for histogram to calculate histogram
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-kde</option></term>
        <term><option>--kde</option></term>
        <listitem>
          <para>
            Use
            <ulink url="http://pktools.nongnu.org/html/classKernel.html">Kernel</ulink>
            density estimation when producing histogram.
            The standard deviation is estimated based on Silverman's rule of
            thumb
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cor</option></term>
        <term><option>--correlation</option></term>
        <listitem>
          <para>
            calculate Pearson produc-moment correlation coefficient between
            two columns (defined by <option>-c</option> &lt;col1&gt;
            <option>-c</option> &lt;col2&gt;)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-rmse</option></term>
        <term><option>--rmse</option></term>
        <listitem>
          <para>
            calculate root mean square error between two columns (defined by
            <option>-c</option> &lt;col1&gt; <option>-c</option> &lt;col2&gt;)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-reg</option></term>
        <term><option>--regression</option></term>
        <listitem>
          <para>
            calculate linear regression between two columns and get
             correlation coefficient (defined by
             <option>-c</option> &lt;col1&gt;
             <option>-c</option> &lt;col2&gt;)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-regerr</option></term>
        <term><option>--regerr</option></term>
        <listitem>
          <para>
            calculate linear regression between two columns and get root mean
            square error (defined by <option>-c</option> &lt;col1&gt;
            <option>-c</option> &lt;col2&gt;)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-v</option> <replaceable>level</replaceable></term>
        <term><option>--verbose</option> <replaceable>level</replaceable></term>
        <listitem>
          <para>
            verbose mode when positive
          </para>
        </listitem>
      </varlistentry>

    </variablelist>
    
    <para>Advanced options</para>
    <variablelist>

      <varlistentry>
        <term><option>-src_min</option> <replaceable>value</replaceable></term>
        <term><option>--src_min</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            start reading source from this minimum value
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-src_max</option> <replaceable>value</replaceable></term>
        <term><option>--src_max</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            stop reading source from this maximum value
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-fs</option> <replaceable>separator</replaceable></term>
        <term><option>--fs</option> <replaceable>separator</replaceable></term>
        <listitem>
          <para>
            field separator.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-r</option> <replaceable>startrow</replaceable> [<option>-r</option> <replaceable>endrow</replaceable>]</term>
        <term><option>--range</option> <replaceable>startrow</replaceable> [<option>--range</option> <replaceable>endrow</replaceable>]</term>
        <listitem>
          <para>
            rows to start/end reading.
            Use <option>-r</option> 1 <option>-r</option> 10 to read first
            10 rows where first row is header.
            Use 0 to read all rows with no header.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-o</option></term>
        <term><option>--output</option></term>
        <listitem>
          <para>
            output the selected columns
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-t</option></term>
        <term><option>--transpose</option></term>
        <listitem>
          <para>
            transpose input ascii vector
            (use in combination with <option>--output</option>)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-comment</option> <replaceable>character</replaceable></term>
        <term><option>--comment</option> <replaceable>character</replaceable></term>
        <listitem>
          <para>
            comment character
          </para>
        </listitem>
      </varlistentry>

    </variablelist>

  </refsect1>

</refentry>
