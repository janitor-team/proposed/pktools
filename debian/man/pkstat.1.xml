<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd">
<refentry id='pkstat'>

  <refmeta>
    <refentrytitle>pkstat</refentrytitle>
    <manvolnum>1</manvolnum>
  </refmeta>

  <refnamediv>
    <refname>pkstat</refname>
    <refpurpose>calculate basic statistics from raster dataset</refpurpose>
  </refnamediv>

  <refsynopsisdiv id='synopsis'>
    <cmdsynopsis>
      <command>pkstat</command>
      <arg choice='opt'><replaceable>options</replaceable></arg>
    </cmdsynopsis>
  </refsynopsisdiv>

  <refsect1 id='description'>
    <title>DESCRIPTION</title>
    <para>
      <command>pkstat</command> is utility to calculate basic statistics from
      a raster dataset.
    </para>
  </refsect1>

  <refsect1 id='options'>
    <title>OPTIONS</title>
    <variablelist>

      <varlistentry>
        <term><option>-i</option> <replaceable>filename</replaceable></term>
        <term><option>--input</option> <replaceable>filename</replaceable></term>
        <listitem>
          <para>
            name of the input raster dataset
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-b</option> <replaceable>band</replaceable></term>
        <term><option>--band</option> <replaceable>band</replaceable></term>
        <listitem>
          <para>
            band(s) on which to calculate statistics
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-f</option></term>
        <term><option>--filename</option></term>
        <listitem>
          <para>
            Shows image filename
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-stats</option></term>
        <term><option>--statistics</option></term>
        <listitem>
          <para>
            Shows basic statistics
            (calculate in memory)
            (min,max, mean and stdDev of the raster datasets)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-fstats</option></term>
        <term><option>--fstatistics</option></term>
        <listitem>
          <para>
            Shows basic statistics using GDAL computeStatistics
            (min,max, mean and stdDev of the raster datasets)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-ulx</option> <replaceable>value</replaceable></term>
        <term><option>--ulx</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Upper left x value bounding box
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-uly</option> <replaceable>value</replaceable></term>
        <term><option>--uly</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Upper left y value bounding box
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-lrx</option> <replaceable>value</replaceable></term>
        <term><option>--lrx</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Lower right x value bounding box
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-lry</option> <replaceable>value</replaceable></term>
        <term><option>--lry</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Lower right y value bounding box
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-nodata</option> <replaceable>value</replaceable></term>
        <term><option>--nodata</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Set nodata value(s)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-down</option> <replaceable>value</replaceable></term>
        <term><option>--down</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            Down sampling factor (for raster sample datasets only).
            Can be used to create grid points.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-rnd</option> <replaceable>number</replaceable></term>
        <term><option>--rnd</option> <replaceable>number</replaceable></term>
        <listitem>
          <para>
            generate random numbers
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-mean</option></term>
        <term><option>--mean</option></term>
        <listitem>
          <para>
            calculate mean
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-median</option></term>
        <term><option>--median</option></term>
        <listitem>
          <para>
            calculate median
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-var</option></term>
        <term><option>--var</option></term>
        <listitem>
          <para>
            calculate variance
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-skew</option></term>
        <term><option>--skewness</option></term>
        <listitem>
          <para>
            calculate skewness
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-kurt</option></term>
        <term><option>--kurtosis</option></term>
        <listitem>
          <para>
            calculate kurtosis
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-stdev</option></term>
        <term><option>--stdev</option></term>
        <listitem>
          <para>
            calculate standard deviation
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-sum</option></term>
        <term><option>--sum</option></term>
        <listitem>
          <para>
            calculate sum of column
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-mm</option></term>
        <term><option>--minmax</option></term>
        <listitem>
          <para>
            calculate minimum and maximum value
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-min</option></term>
        <term><option>--min</option></term>
        <listitem>
          <para>
            calculate minimum value
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-max</option></term>
        <term><option>--max</option></term>
        <listitem>
          <para>
            calculate maximum value
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-src_min</option> <replaceable>value</replaceable></term>
        <term><option>--src_min</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            start reading source from this minimum value
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-src_max</option> <replaceable>value</replaceable></term>
        <term><option>--src_max</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            stop reading source from this maximum value
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-hist</option></term>
        <term><option>--hist</option></term>
        <listitem>
          <para>
            calculate histogram
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-hist2d</option></term>
        <term><option>--hist2d</option></term>
        <listitem>
          <para>
            
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-nbin</option> <replaceable>value</replaceable></term>
        <term><option>--nbin</option> <replaceable>value</replaceable></term>
        <listitem>
          <para>
            number of bins to calculate histogram
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-rel</option></term>
        <term><option>--relative</option></term>
        <listitem>
          <para>
            use percentiles for histogram to calculate histogram
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-kde</option></term>
        <term><option>--kde</option></term>
        <listitem>
          <para>
            Use Kernel density estimation when producing histogram.
            The standard deviation is estimated based on Silverman's rule of thumb.
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-cor</option></term>
        <term><option>--correlation</option></term>
        <listitem>
          <para>
            calculate Pearson produc-moment correlation coefficient between
            two raster datasets (defined by
            <option>-c</option> <replaceable>col1</replaceable>
            <option>-c</option> <replaceable>col2</replaceable>)
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-rmse</option></term>
        <term><option>--rmse</option></term>
        <listitem>
          <para>
            calculate root mean square error between two raster datasets
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-reg</option></term>
        <term><option>--regression</option></term>
        <listitem>
          <para>
            calculate linear regression between two raster datasets and get
            correlation coefficient
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-regerr</option></term>
        <term><option>--regerr</option></term>
        <listitem>
          <para>
            calculate linear regression between two raster datasets and get
            root mean square error
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-preg</option></term>
        <term><option>--preg</option></term>
        <listitem>
          <para>
            calculate perpendicular regression between two raster datasets and
            get correlation coefficient
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-pregerr</option></term>
        <term><option>--pregerr</option></term>
        <listitem>
          <para>
            calculate perpendicular regression between two raster datasets and
            get root mean square error
          </para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term><option>-v</option> <replaceable>level</replaceable></term>
        <term><option>--verbose</option> <replaceable>level</replaceable></term>
        <listitem>
          <para>
            verbose mode when positive
          </para>
        </listitem>
      </varlistentry>

    </variablelist>
  </refsect1>

</refentry>
