#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# Enable hardening build flags
export DEB_BUILD_MAINT_OPTIONS=hardening=+all

include /usr/share/dpkg/pkg-info.mk

UPSTREAM_VERSION = $(shell echo $(DEB_VERSION_UPSTREAM) | sed -e 's/\+.*//')

BUILD_DATE=$(shell LC_ALL=C date -u "+%d %B %Y" -d "@$(SOURCE_DATE_EPOCH)")

ifneq ($(wildcard /usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/serial/libhdf5.so),)
	export DEB_CXXFLAGS_MAINT_APPEND=-DNDEBUG -I/usr/include/hdf5/serial
else
	export DEB_CXXFLAGS_MAINT_APPEND=-DNDEBUG
endif

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

MANPAGES:=$(wildcard debian/man/*.*.xml)

%:
	dh $@ --buildsystem cmake \
	      --with pkgkde_symbolshelper

override_dh_auto_configure:
	dh_auto_configure -- -DBUILD_WITH_FANN=ON \
	                     -DBUILD_SHARED_LIBS=ON \
	                     -DINSTALL_LIBRARY_DIR=/usr/lib/$(DEB_HOST_MULTIARCH)

override_dh_clean:
	dh_clean debian/man/*.1
	find $(CURDIR) -name '*.o' -delete
	find $(CURDIR) -name '*.so*' -delete
	find $(CURDIR) -name '*.a' -delete

override_dh_auto_build:
	# Create man pages from DocBook XML
	for x in $(MANPAGES) ; do \
	  docbook2x-man --string-param header-3="$(BUILD_DATE)" $$x ; \
	  mv `basename $$x | sed 's/.xml$$//'` `dirname $$x` ; \
	done

	dh_auto_build

override_dh_makeshlibs:
	dh_makeshlibs -- -v$(UPSTREAM_VERSION) -c0
